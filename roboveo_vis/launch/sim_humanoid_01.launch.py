import launch
from launch_ros.actions import Node


def generate_launch_description():
    return launch.LaunchDescription([
        Node(
            package="roboveo",
            executable="roboveo_node",
            parameters=[{
                "screen_update_rate": 30.0
            }],
            namespace="sim_humanoid_01",
            remappings=[
                ("/tf", "/sim_humanoid_01/tf"),
                ("/tf_static", "/sim_humanoid_01/tf_static")]
        )
    ])
