# -*- coding: utf-8 -*-

# Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import time
import numpy as np
import meshcat
import meshcat.transformations as tf
from roboveo.objects import KinematicChain


def example():

    kinematic_conf = [
        {
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "link_tf": tf.translation_matrix([0.2, 0, 0])
        },
        {
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "link_tf": np.dot(
                tf.rotation_matrix(1, [0.2, 0.2, 0.2]),
                tf.translation_matrix([0.2, 0, 0]))
        },
        {
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "link_tf": tf.translation_matrix([0.3, 0.2, -0.1])
        },
        {
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "link_tf": tf.translation_matrix([0.1, 0.1, 0])
        }
    ]
    vis = meshcat.Visualizer()
    chain = KinematicChain("chain", vis, kinematic_conf)

    i = 0
    while True:
        i += 1
        time.sleep(0.1)
        joints = [np.sin(0.1*i), np.cos(0.2*i), np.cos(0.3*i), np.sin(0.1*i)]
        chain.apply_configuration(joints)


if __name__ == "__main__":
    example()
