# -*- coding: utf-8 -*-

# Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import time
import numpy as np
import meshcat
import meshcat.transformations as tf
from roboveo.objects import Segment, Frame


def example():
    vis = meshcat.Visualizer()
    frame = Frame("frame", vis)
    segment = Segment("segment", vis, action_axis = np.array([0, 0, 1]),
                      link_transform=tf.translation_matrix([0, 0.3, 0.3]))

    for i in range(1000):
        time.sleep(0.1)
        segment.apply_local_transform(i * np.pi /180)


if __name__ == "__main__":
    example()
