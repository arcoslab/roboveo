# -*- coding: utf-8 -*-

# Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import time
import numpy as np
from numpy import pi
import meshcat
import meshcat.transformations as tf
from roboveo.objects import KinematicChain


def euler_trans_pose(roll, pitch, yaw, x_t, y_t, z_t):
    return np.dot(
        tf.translation_matrix([x_t, y_t, z_t]),
        tf.euler_matrix(roll, pitch, yaw)
    )

def example():


    kinematic_conf = [
        {
            "tag": "torso0",
            "axis": np.array([1, 0, 0]),
            "type": "static",
            "scale": 1,
            "link_tf": euler_trans_pose(pi/2, pi/2, pi/2, 0, 0, 1.23)
        },
        {
            "tag": "torso1",
            "axis": np.array([1, 0, 0]),
            "type": "static",
            "scale": 1,
            "link_tf": euler_trans_pose(0, 0, 0, 0, 0, 0.11)
        },
        {
            "tag": "segment0",
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "scale": 1,
            "link_tf": euler_trans_pose(-pi/2, 0, 0, 0, 0, 0.2)
        },
        {
            "tag": "segment1",
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "scale": -1,
            "link_tf": euler_trans_pose(pi/2, 0, 0, 0, -0.2, 0)
        },
        {
            "tag": "segment2",
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "scale": 1,
            "link_tf": euler_trans_pose(pi/2, 0, 0, 0, 0, 0.2)
        },
        {
            "tag": "segment3",
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "scale": -1,
            "link_tf": euler_trans_pose(-pi/2, 0, 0, 0, 0.2, 0)
        },
        {
            "tag": "segment4",
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "scale": 1,
            "link_tf": euler_trans_pose(-pi/2, 0, 0, 0, 0, 0.19)
        },
        {
            "tag": "segment5",
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "scale": -1,
            "link_tf": euler_trans_pose(pi/2, 0, 0, 0, -0.078, 0)
        },
        {
            "tag": "segment6",
            "axis": np.array([0, 0, 1]),
            "type": "rotational",
            "scale": 1,
            "link_tf": euler_trans_pose(
                pi/2, pi/4, -pi/4, -0.075, -0.075, -0.094)
        }
    ]

    vis = meshcat.Visualizer()
    chain = KinematicChain("chain", vis, kinematic_conf)

    i = 0
    while True:
        i += 1
        time.sleep(0.1)
        joints = [np.sin(0.1*i), np.cos(0.2*i), np.cos(0.3*i), np.sin(0.1*i),
                  np.sin(0.1*i), np.cos(0.2*i), np.sin(0.2*i)]

        chain.apply_configuration(joints)


if __name__ == "__main__":
    example()

