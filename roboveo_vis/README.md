# Roboveo

A simple robot visualization tool

## Installation

Follow arcos-lab instructions for installing the humanoid robot simulator
[here](https://wiki.arcoslab.org/en/tutorials/cog_arch). 

- Make sure to set up your ROS2 workspace correctly
- Source the setup file of your workspace and activate the python virtual
  environment
- Clone this repo into your workspace
- Colcon build

There are some python specific dependencies but they should be already installed
when you installed the simulator

## Executing

```
ros2 run roboveo roboveo_node --ros-args -r _ns:=<namespace>
```


Open a browser tab a go to [127.0.0.1:7000/static/](127.0.0.1:7000/static/)
