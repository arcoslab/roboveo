from setuptools import setup
import os
from glob import glob

package_name = "roboveo"

setup(
    name=package_name,
    version='0.3.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, "config"), glob('config/*.yaml')),
        (os.path.join('share', package_name, "meshes"), glob('meshes/*.dae')),
        (os.path.join('share', package_name), glob('launch/*.launch.py')),
    ],
    install_requires=[
        'setuptools',
        'urchin',
        'meshcat',
        'numpy'],
    zip_safe=True,
    maintainer='Daniel Garcia-Vaglio',
    maintainer_email='degv364@gmail.com',
    description='Simple robot visualization tool',
    license='GPLv3+',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'roboveo_node = roboveo.roboveo_node:main',
            'scene_loader = roboveo.scene_loader:main'
        ],
    },
)
