# Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


from io import StringIO

import numpy as np
import rclpy
from urchin import URDF
from time import time
from tf2_ros import TransformException

from rbc.pythonic_kdl import Frame
from roboveo.link import BaseLink, RoboveoLink
from roboveo.utils import _load_mesh, _from_ros_resource
from roboveo.urdf_tree import TreeFromURDF


def _get_scale(candidate):
    if candidate is None:
        return [1.0, 1.0, 1.0]
    else:
        return list(candidate)


def _get_origin(candidate):
    if candidate is None:
        return np.identity(4)
    else:
        return candidate


def _create_file(description):
    """
    The library for URDF that we are using only loads from files
    and not directly from strings like a ROS2 node has to do.
    So this creates a temporary file to be able to load
    """
    name = "/tmp/"+str(time())+".urdf"
    temp_file = StringIO(description)
    temp_file.__setattr__("name", name)
    return temp_file


class RoboticBody():
    def __init__(self, meshcat_vis, description, position_delta,
                 orientation_delta):
        self.vis = meshcat_vis
        self.base_link = None
        self._links = {}
        self.ready = False
        self.pos_delta = position_delta
        self.rot_delta = orientation_delta
        self.load_urdf(description)

    def add_link(self, name):
        path = self.tree.paths[name]
        parent = self.tree.relations[name]
        link = RoboveoLink(name, self.vis, path, parent)
        self._links[name] = link

    def get_link(self, name):
        return self._links[name]

    def load_urdf(self, description):
        temp_file = _create_file(description)
        robot_description = URDF.load(temp_file, lazy_load_meshes=True)
        self.tree = TreeFromURDF(robot_description)
        self.base_link = BaseLink(robot_description.base_link.name, self.vis)
        for link in robot_description.links:
            if len(link.visuals) != 0:
                self.add_link(link.name)
                for visual in link.visuals:
                    mesh = _load_mesh(
                        _from_ros_resource(visual.geometry.mesh.filename))
                    scale = _get_scale(visual.geometry.mesh.scale)
                    origin = _get_origin(visual.origin)
                    self.get_link(link.name).add_mesh(
                        mesh, scale, origin)

    def update(self, tf_buffer, logger=None):
        if not self.ready:
            return
        elapsed_times = []
        update_times = []
        big_start = time()
        for link_name in self._links:
            try:
                start_time = time()
                transform_ros_stamped = tf_buffer.lookup_transform(
                    self._links[link_name].parent,
                    link_name,
                    rclpy.time.Time())
                end_time = time()
            except TransformException as ex:
                if logger is not None:
                    logger.info(
                        f"Error in transform from {self.base_link.name} to {link_name}: {ex}")
            else:
                elapsed_times.append(end_time - start_time)
                transform = Frame.build_from_ros2_transform(
                    transform_ros_stamped.transform)
                start_update_time = time()
                self.get_link(link_name).update(
                    transform, self.pos_delta, self.rot_delta)
                update_times.append(time()-start_update_time)

        logger.debug("All: " + str(time()-big_start))
        logger.debug("Updates: " + str(sum(update_times)))
        logger.debug("just tf: " + str(sum(elapsed_times)))
