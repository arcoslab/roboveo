# Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from numpy.linalg import norm
from ament_index_python.packages import get_package_share_directory
import meshcat.geometry as geo
from rbc.pythonic_kdl import diff

def _from_ros_resource(resource_filename):
    """
    Xacro produces filenames in the ROS resource format,
    we want normal filenames instead
    """
    clean = resource_filename.replace("package://", "")
    pkg_name = clean.split("/")[0]

    pkg_share_dir = get_package_share_directory(pkg_name)

    complete_path = clean.replace(pkg_name, pkg_share_dir)

    return complete_path


def _load_mesh(filename):
    file_type = filename.split(".")[-1]
    if file_type == "dae":
        mesh = geo.DaeMeshGeometry.from_file(filename)
    elif file_type == "stl":
        mesh = geo.StlMeshGeometry.from_file(filename)
    elif file_type == "obj":
        mesh = geo.ObjMeshGeometry.from_file(filename)
    else:
        raise NotImplementedError("No loader for ."+file_type)

    return mesh


def _is_close(prev, current, pos_delta, rot_delta):
    twist = diff(prev, current)
    return norm(twist[:3]) < pos_delta and norm(twist[3:]) < rot_delta
