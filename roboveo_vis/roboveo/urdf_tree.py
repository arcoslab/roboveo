# Copyright (c) 2019-2024 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


class TreeFromURDF():
    def __init__(self, urdf):
        self.tree = {}
        self.relations = {}
        self.paths = {"": ""}
        self.link_names = [link.name for link in urdf.links]
        self.joint_relations = [
            (joint.parent, joint.child) for joint in urdf.joints]
        base_name = urdf.base_link.name
        self.urdf = urdf

        self.add_children(base_name, "", self.tree)

        self.paths.pop("")
        self.relations.pop(base_name)

    def find_children(self, link_name):
        children = []
        for parent, child in self.joint_relations:
            if parent == link_name:
                children.append(child)
        return children

    def add_children(self, link_name, parent_name, dict_so_far):
        children = self.find_children(link_name)
        if len(children) != 0:
            if self.has_mesh(link_name):
                self.relations[link_name] = parent_name
                self.paths[link_name] = self.paths[parent_name] + "/" + link_name
                dict_so_far[link_name] = {}
                for child in children:
                    self.add_children(child, link_name, dict_so_far[link_name])
            else:
                for child in children:
                    self.add_children(child, parent_name, dict_so_far)
        else:
            if self.has_mesh(link_name):
                self.relations[link_name] = parent_name
                self.paths[link_name] = self.paths[parent_name] + "/" + link_name
                dict_so_far[link_name] = None

    def has_mesh(self, link_name):
        if link_name == self.urdf.base_link.name:
            return True
        for link in self.urdf.links:
            if link.name == link_name:
                return len(link.visuals) != 0
