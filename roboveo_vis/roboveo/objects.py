# -*- coding: utf-8 -*-

# Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import numpy as np
import rclpy

import meshcat.geometry as geo
import meshcat.transformations as tf

from tf2_ros import TransformException

from roboveo.mesh import RoboveoMesh
from roboveo.utils import _load_mesh
from rbc.pythonic_kdl import Frame as RbcFrame


def get_transform_for_direction(current_axis, required_axis):

    angle = tf.angle_between_vectors(current_axis, required_axis)
    if angle == 0:
        return np.identity(4)
    if angle == np.pi:
        if np.dot(current_axis, np.array([1, 0, 0])) != 0:
            rot_axis = np.array([1, 0, 0])
        else:
            rot_axis = np.array([0, 1, 0])
    else:
        rot_axis = np.cross(current_axis, required_axis)

    return tf.rotation_matrix(angle, rot_axis)


class Arrow():
    """
    Draw an arrow of specified length and radius
    """
    def __init__(self, name, parent, color=0xff0000,
                 length=1, radius=0.2, axis=np.array([0, 0, 1])):
        """
        Arrow constructor

        :param name: string with the name of the leaf at which this object is
        :param parent: Meshcat geometry to wich this arrow is attached
        :param color: Hexadecimal with color
        :param length: Length of the cylinder enclosing the arrow in meters
        :param radius: Radius of the cylinder enclosing the arrow in meters
        :param axis: 3D with the direction fo the axis
        """
        # For some reason meshcat uses 'y' as above, we want to use 'z' as above
        y_axis = np.array([0, 1, 0])
        rotation_transform = get_transform_for_direction(y_axis, axis)

        # Leaf at wich this object is located
        self.leaf = parent[name]
        # Creat ethe body of the arrow
        self.leaf["body"].set_object(
            geo.Cylinder(length*2/3, radius=radius/2),
            geo.MeshLambertMaterial(color=color, reflectivity=0.8))

        # Rotate body to face Z axis
        self.leaf["body"].set_transform(np.dot(
            tf.translation_matrix(length * axis / 3),
            rotation_transform
        ))

        # Create the head of the arrow, which is a cone
        self.leaf["head"].set_object(
            geo.Cylinder(length/3, radiusTop=0, radiusBottom=radius),
            geo.MeshLambertMaterial(color=0xffffff, reflectivity=0.8))

        # Put the head at the correct pose
        self.leaf["head"].set_transform(np.dot(
            tf.translation_matrix(axis * length * 2 / 3),
            rotation_transform
        ))

    def __del__(self):
        self.leaf.delete()


class Frame():
    """
    Draw a frame, which is an arrow pointing to each positive axis
    """
    def __init__(self, name, parent, size=1):
        """
        Frame constructor

        :param name: string with the name of the leaf at which this object is
        :param parent: Meshcat geometry to wich this arrow is attached
        :param size: size of cube enlosing the frame
        """
        self.leaf = parent[name]

        # Create arrows
        self.z_arrow = Arrow("z", self.leaf, color=0x0000ff, length=size,
                             radius=size/5, axis=np.array([0, 0, 1]))

        self.x_arrow = Arrow("x", self.leaf, color=0xff0000, length=size,
                             radius=size/5, axis=np.array([1, 0, 0]))

        self.y_arrow = Arrow("y", self.leaf, color=0x00ff00, length=size,
                             radius=size/5, axis=np.array([0, 1, 0]))

    def __del__(self):
        self.leaf.delete()


class MeshObject():
    """
    Draw an arbitrary object from a Mesh
    """
    def __init__(
            self, name, filename, pose, scale, listen_tf,
            meshcat_vis, root_frame=""):
        self.name = name
        if isinstance(pose, np.ndarray):
            self.pose = pose
        else:
            self.pose = RbcFrame.build_from_ros2_pose(pose)
        self.leaf = meshcat_vis[name]
        if isinstance(scale, list):
            self.scale = scale
        else:
            self.scale = [scale.x, scale.y, scale.z]
        self.mesh = RoboveoMesh(
            mesh=_load_mesh(filename),
            scale=self.scale,
            origin=np.identity(4),
            meshcat_link=self.leaf,
            index=0)
        print(self.pose)
        self.leaf.set_transform(self.pose)
        self.mesh.update()
        self.listen_tf = listen_tf
        self.root_frame = root_frame

    def update(self, tf_buffer, logger=None):
        if not self.listen_tf:
            self.mesh.update()
            return
        try:
            transform_ros_stamped = tf_buffer.lookup_transform(
                self.root_frame,
                self.name,
                rclpy.time.Time()
            )
        except TransformException as ex:
            if logger is not None:
                logger.info(
                    f"Error in transform from {self.root_frame} to {self.name}: {ex}"
                )
        else:
            self.pose = RbcFrame.build_from_ros2_transform(
                transform_ros_stamped.transform)
            self.leaf.set_transform(self.pose)
            self.mesh.update()

    @classmethod
    def from_roboveo_request(cls, request, meshcat_vis):
        return cls(
            name=request.name,
            filename=request.mesh_filename,
            pose=request.pose,
            scale=request.scale,
            listen_tf=request.listen_tf,
            meshcat_vis=meshcat_vis,
            root_frame=request.root_frame)
