# Copyright (c) 2019-2025 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


from roboveo.mesh import RoboveoMesh
from roboveo.utils import _is_close
from rbc.pythonic_kdl import Frame


class BaseLink():
    def __init__(self, name, meshcat_vis):
        self.leaf = meshcat_vis[name]
        self.children = []
        self.name = name

    def add_child(self, child):
        self.children.append(child)


class RoboveoLink():

    def __init__(self, name, vis, path, parent):
        self.name = name
        self.parent = parent
        self.leaf = vis[path]
        # parent.add_child(self)
        self.children = []
        self.meshes = []
        self.prev_tf = Frame()

    def add_child(self, child):
        self.children.append(child)

    def add_mesh(self, mesh, scale, origin):
        index = len(self.meshes)
        self.meshes.append(
            RoboveoMesh(
                mesh=mesh, scale=scale, origin=origin,
                meshcat_link=self.leaf,
                index=index))

    def update(self, transform, pos_delta, rot_delta):
        if not _is_close(transform, self.prev_tf, pos_delta, rot_delta):
            self.leaf.set_transform(transform)
            self.prev_tf = transform
        for mesh in self.meshes:
            mesh.update()
