# Copyright (c) 2019-2025 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import yaml

import rclpy

from rclpy.node import Node
from roboveo_msgs.srv import AddObject
from rbc.pythonic_kdl import Frame
from geometry_msgs.msg import TransformStamped
from numpy.random import random

from tf2_ros import TransformBroadcaster

from ament_index_python.packages import get_package_share_directory


def _expand_filename(name):
    share_dir = get_package_share_directory("roboveo")
    return share_dir+"/meshes/"+name


class SceneLoader(Node):
    def __init__(self, name="scene_loader_tester"):
        super().__init__(name)

        self.adder_client = self.create_client(
            AddObject, "roboveo_node/add_object")
        while not self.adder_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info(
                "Service not yet ready, make sure roboveo is up")
        self.req = AddObject.Request()
        self.ready = False
        self.transforms = []

        self.tf_pub = TransformBroadcaster(node=self)
        self.timer = self.create_timer(1.0, self.random_pose)

    def load_scene(self, scene_description):
        for object_name in scene_description["objects"]:
            obj_desc = scene_description["objects"][object_name]
            self.req.name = object_name
            self.req.mesh_filename = _expand_filename(
                obj_desc["mesh_filename"])
            self.req.scale.x = obj_desc["scale"][0]
            self.req.scale.y = obj_desc["scale"][1]
            self.req.scale.z = obj_desc["scale"][2]
            self.req.listen_tf = obj_desc["listen_tf"]

            self.req.pose.position.x = obj_desc["pose"]["position"][0]
            self.req.pose.position.y = obj_desc["pose"]["position"][1]
            self.req.pose.position.z = obj_desc["pose"]["position"][2]

            self.req.pose.orientation.x = obj_desc["pose"]["orientation"][0]
            self.req.pose.orientation.y = obj_desc["pose"]["orientation"][1]
            self.req.pose.orientation.z = obj_desc["pose"]["orientation"][2]
            self.req.pose.orientation.w = obj_desc["pose"]["orientation"][3]

            if self.req.listen_tf:
                self.req.root_frame = obj_desc["root_frame"]
                tfst = TransformStamped()
                tfst.header.stamp = self.get_clock().now().to_msg()
                tfst.header.frame_id = obj_desc["root_frame"]
                tfst.child_frame_id = object_name
                tfst.transform = Frame.build_from_ros2_pose(self.req.pose).to_ros2_transform()
                self.transforms.append(tfst)
            else:
                self.req.root_frame = ""

            future = self.adder_client.call_async(self.req)
            rclpy.spin_until_future_complete(self, future)

        self.ready = True

    def random_pose(self):
        if not self.ready:
            return
        for tfst in self.transforms:
            tfst.header.stamp = self.get_clock().now().to_msg()
            tfst.transform.translation.x += 0.1*random()-0.05
            tfst.transform.translation.y += 0.1*random()-0.05
            tfst.transform.translation.z += 0.1*random()-0.05
            self.tf_pub.sendTransform(tfst)


def main(args=None):
    rclpy.init(args=args)
    node = SceneLoader()
    fi = open(get_package_share_directory("roboveo")+"/config/default_arcos_scene.yaml")
    description = yaml.safe_load(fi)
    fi.close()
    node.load_scene(description)
    rclpy.spin(node)
    rclpy.shutdown()
