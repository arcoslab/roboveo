# Copyright (c) 2019-2025 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import rclpy
import meshcat
from rclpy.node import Node
from std_msgs.msg import String as StringMsg

from rclpy.qos import QoSProfile, DurabilityPolicy

from math import radians

from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener

from roboveo.robotic_body import RoboticBody
from roboveo.objects import MeshObject
from roboveo_msgs.srv import AddObject


class RoboveoNode(Node):

    def __init__(self):
        super().__init__("roboveo_node")
        self.vis = meshcat.Visualizer()
        self.robot_descriptions = []
        self.objects = []

        self.declare_parameter("screen_update_rate", 30.0)
        self.screen_update_rate = float(self.get_parameter(
            "screen_update_rate").value)

        self.declare_parameter("position_delta", 0.01)
        self.position_delta = self.get_parameter("position_delta").value

        self.declare_parameter("orientation_delta", radians(1))
        self.orientation_delta = self.get_parameter("orientation_delta").value

        # TF2
        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)

        qpro = QoSProfile(depth=10)
        qpro.durability = DurabilityPolicy.TRANSIENT_LOCAL
        self.create_subscription(
            StringMsg, "robot_description", self.get_description_cb, qpro)
        self.screen_timer = self.create_timer(
            1/self.screen_update_rate, self.update_screen)

        self.object_add_srv = self.create_service(
            AddObject, self.get_name()+"/add_object", self.add_object)

    def get_description_cb(self, msg):
        description = msg.data
        current = RoboticBody(
            self.vis, description, self.position_delta, self.orientation_delta)
        self.robot_descriptions.append(current)
        current.ready = True

    def add_object(self, request, response):
        self.objects.append(MeshObject.from_roboveo_request(request, self.vis))
        response.successful = True
        response.reason = ""
        return response

    def update_screen(self):
        for robo_desc in self.robot_descriptions:
            robo_desc.update(self.tf_buffer, logger=self.get_logger())
        for obj in self.objects:
            obj.update(self.tf_buffer, logger=self.get_logger())


def main(args=None):
    rclpy.init(args=args)
    roboveo_node = RoboveoNode()
    rclpy.spin(roboveo_node)
    rclpy.shutdown()
