# Copyright (c) 2019-2025 Autonomous Robots and Cognitive Systems Laboratory
# Universidad de Costa Rica
# Authors: Daniel Garcia Vaglio degv364@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


class RoboveoMesh():
    def __init__(self, mesh, scale, origin, meshcat_link, index):
        self.mesh = mesh
        self.scale = scale
        self.origin = origin

        self.link = meshcat_link
        self.leaf = meshcat_link["mesh_"+str(index)]
        self.leaf.set_object(self.mesh)

        self.leaf.set_transform(self.origin)
        self.update()

    def update(self):
        if self.scale not in [[1, 1, 1], [1.0, 1.0, 1.0]]:
            self.leaf.set_property("scale", self.scale)
